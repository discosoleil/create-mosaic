#@ File (label="Image tiles root folder", style="directory") fdata
#@ File (label="Dark images root folder", style="directory") fdark
#@ File (label="Dark of White images root folder", style="directory") fdarkwhite
#@ File (label="White z-stack images root folder", style="directory") fwhite

#@ OpService ops
#@ ConvertService convertService
#@ DatasetService datasetService

"""
Jython script to reconstruc mosaic from telemos multi-roi acquisitions.
This script should be launched in Fiji. Drag the file to Fiji main windows 
and launch it from the script editor by pressing F5. Or using the Run button.

@author: H. Chauvet (synchrotron-soleil)
Used IJ plugins: Bio-Formats, BaSIC
"""

import glob
from ij import IJ, ImagePlus, ImageStack
from ij.process import FloatProcessor
import os
import json 
from ij.gui import NonBlockingGenericDialog, GenericDialog, DialogListener
from fiji.util.gui import GenericDialogPlus
from java.awt.event import WindowAdapter
from java.lang import Thread
from ij import WindowManager
import re
import sys

try:
   basedir = os.path.dirname(__file__)
except:
    basedir = os.path.expanduser("~/Documents/ImageJ-Script")
    print(basedir)

sys.path.append(basedir)
from utils.ImagesLoaders import load_image_sequence

# Import to do operation with ImageJ2 and ImgMath
from net.imglib2.util import Intervals
from net.imglib2.img.display.imagej import ImageJFunctions as IL
from net.imglib2.algorithm.math.ImgMath import computeIntoFloats, sub, div, GT, LT, IF, THEN, ELSE, minimum
from net.imglib2.type.numeric.real import FloatType
from net.imglib2.view import Views 
from net.imagej import Dataset
from net.imagej.axis import Axes

def getFirstMetadata(metadata):
    """
    Return the first keys starting with Metadata
    """
    ml = list(metadata)
    cpt = 0
    for l in ml:
        cpt += 1
        if l.startswith('Metadata'):
            break
    
    return cpt
    
def getMMversion(metadata):
    """
    Get MM version from metadata 
    """

    if int(metadata['Summary']["MicroManagerVersion"].split('.')[0]) >= 2:
        version = 2
    else:
        version = 1

    return version


def getRoi(metadata, mm_version=2):
    """
    Return the ROI rectangle from metadata file
    """

    if mm_version == 2:
        # Use the first image to get the ROI
        i = getFirstMetadata(metadata)
        roi = metadata[metadata.keys()[i]]['ROI']
    else:
        roi = metadata['Summary']['ROI']

    roi = [int(r) for r in roi.split('-')]
    return roi


def getScale(metadata):
    """
    Return the scale from metadata of MM
    """

    try:
        # MM2 version
        i = getFirstMetadata(metadata)
        scale=metadata[metadata.keys()[i]]['PixelSizeUm']
    except:
        # MM1 version
        scale = metadata['Summary']['PixelSize_um']
        
    return scale

def getWidthHeight(metadata):
    """
    Get the width and height from metadata
    """
    try:
        # MM2 version
        i = getFirstMetadata(metadata)
        height = metadata[metadata.keys()[i]]['Height']
        width = metadata[metadata.keys()[i]]['Width']
    except:
        # MM1 version
        height = metadata['Summary']['Height']
        width = metadata['Summary']['Width']
        
    return width, height
    
def load_image_tiles_stack(images_dir):
    """
    Fonction pour charge le stack d'images depuis
    un dossier crée par MM1.4 avec des tuiles.

    Les tuiles peuvent être crées avec:
    - Slide Explorer
    - Outils grille (create grid) du multi position
    """

    d = images_dir

    # Travail sur le nom du repertoir

    # Test if it's a SlideExplorer Acquisition (format roi1_tileXXX)
    directorySE = os.path.join(os.path.abspath(d),'roi*')
    filesSE = glob.glob(directorySE)

    # Test if the format is from create gride (1-Pos_XXX_YYY)
    directoryCG = os.path.join(os.path.abspath(d),'?-Pos_*')
    filesCG = glob.glob(directoryCG)

    if len(filesSE) > 0:
        format = 'SE'
        files = filesSE
        directory = directorySE
        # Regex to count the number of roi in the folder
        regex_cpt_roi = 'roi([0-9]*)_tile'
        name_pattern = 'roi%s_tile'

    if len(filesCG) > 0:
        format = 'CG'
        files = filesCG
        directory = directoryCG
        regex_cpt_roi = '([0-9]*)-Pos_'
        name_pattern = '%s-Pos_'

    # Simple regexp to find roi numbers roiXX_tile
    matched = re.findall(regex_cpt_roi, '\n'.join(files))
    # Convert this to python set (this allow to remove duplicate in roinum and sort tilenumber
    roinum = list(set(matched))

    # Is that a multi-roi record ?
    if len(roinum) > 1:
        guiroi = GenericDialogPlus("Multiple ROI please select one")
        guiroi.addChoice("Choose one roi to process", ["roi%s" % r for r in roinum], "roi"+roinum[0])
        guiroi.showDialog()

        if guiroi.wasOKed():
            selected_roi = roinum[guiroi.getChoices()[0].getSelectedIndex()]
        else:
            selected_roi = roinum[0]

    else:
        selected_roi = roinum[0]

    # Load all files for the selected ROI
    directory = os.path.join(os.path.abspath(d), name_pattern % (selected_roi)+'*')
    files = glob.glob(directory)
    base_dir = os.path.abspath(d)

    # Simple regexp to find tile numbers roi1_tileXX
    if format == 'SE':
        file_pattern = 'roi'+selected_roi+'_tile{tile:d}'
        mmetapath = os.path.join(base_dir,file_pattern.format(tile=1),'metadata.txt')

    if format == 'CG':
        file_pattern = selected_roi+'-Pos_{rows:003d}_{cols:003d}'
        mmetapath = os.path.join(base_dir,file_pattern.format(rows=0, cols=0),'metadata.txt')

    # Chargement des métadonnées MM (juste le resumée de la premiere tuile suifit)
    metadata = json.load(open(mmetapath))
    mmversion = getMMversion(metadata)
    
    # Test de version sur MicroManager
    if mmversion >= 2:
        # Add MM2.0 file structure
        file_pattern = os.path.join(file_pattern, 'img_channel{filter}_position{tile-1:003d}_time000000000_z{z:003d}.tif')
        hstack, good_names = load_image_sequence(base_dir, file_pattern, return_filenames=True)
    else:
    	# Add MM1.4 file structure
    	file_pattern = os.path.join(file_pattern, 'img_000000000_{filter}_{z:003d}.tif')
    	hstack, good_names = load_image_sequence(base_dir, file_pattern, return_filenames=True)

    
    hstack.setTitle("Data")

    return hstack, metadata, base_dir, selected_roi, good_names

def load_images(images_dir, roi=None, virtual=False):
    d = os.path.abspath(images_dir)
    # MM1.4 pattern for white has a pos0
    if os.path.isdir(os.path.join(d,'Pos0')):
        white_pattern = os.path.join('Pos0','img_000000000_{filter}_{z:003d}.tif')
    else:
        # Use MM2 pattern img_channel000_position000_time000000000_z000.tif
        white_pattern = os.path.join('Default','img_channel{filter}_position000_time000000000_z{z:003d}.tif')
        
    imp = load_image_sequence(d, white_pattern)
    if roi is not None and len(roi) == 4 and imp.width > roi[2] and imp.height > roi[3]:
        IJ.log('Roi %s on image size (%i, %i)' % (str(roi[2])+','+str(roi[3]), imp.width, imp.height))
        imp.setRoi(*roi)
        #IJ.run(imp, "Crop", "")

    return imp

def virtual_stack_to_stack(imgplus):
    """
    Convert a virtual stack to an image stack
    """
    
    oldstack = imgplus.getImageStack()
    nstack = ImageStack(imgplus.width, imgplus.height)
    Nslice = oldstack.getSize()
    for sl in xrange(Nslice):
        IJ.showProgress(sl, Nslice)
        nstack.addSlice(oldstack.getProcessor(sl+1))

    nimp = ImagePlus(imgplus.getTitle()+' copy',
                     nstack)

    return nimp
        
def process_white(implus_white, implus_darkofwhite, zofwhite, roi=None):
    """
    Process of the white stack images of telemos
    and correct if from it's dark side

    Parameters
    ----------

    - implus_white: an ImagePlus Object from ImageJ,
        The white z-stack of telemos
    - implus_darkofwhite: an ImagePlus Object from ImageJ,
            The dark of the white image
    - zofwhite: int,
            The value of the depth Z used to select the image inthe White z-stack
    - roi: list of int (size 4) or None,
            The list defining the ROI to applay on dark and white. If None dont do any crop

    Return
    ------
    - img_white_corr, imagej ImagePlus object,
            The corrected white
    """
    IJ.log("Process white: (WHITE-DARKOFWHITE)/MAX(WHITE-DARKOFWHITE)")
    
    # Convert to IJ2 array
    img_white = IL.wrap(implus_white)
    img_dark = IL.wrap(implus_darkofwhite)

    # Do we need to crop
    if roi is not None and len(roi) == 4 and implus_white.width > roi[2] and implus_white.height > roi[3]:
        xmin, ymin, xmax, ymax = roi
        xmax = xmin + xmax 
        ymax = ymin + ymax 
    else:
        xmin = ymin = 0
        xmax = implus_white.width
        ymax = implus_white.height

    # Select the chanel
    img_whiteC = Views.interval(Views.hyperSlice(img_white, 2, zofwhite-1), 
                                Intervals.createMinMax(xmin, ymin,
                                                       xmax-1, ymax-1))
    img_darkC =  Views.interval(img_dark, 
                                Intervals.createMinMax(xmin, ymin,
                                                       xmax-1, ymax-1))

    # Get min max values
    minwhite = float(ops.run("stats.min", img_whiteC).toString())

    # Substract white and dark
    opsus = sub(img_whiteC, img_darkC)
    whitemindark = computeIntoFloats(opsus)
    maxwhite = float(ops.run("stats.max", whitemindark).toString())

    # Compute the minimum value of the ratio
    ratiomin = minwhite/maxwhite

    # Check if their is negative values -> put them to the minimum of the ratio
    # And normalise by the max value of white the other one
    op = IF(GT(opsus, ratiomin), 
            THEN(div(opsus,maxwhite)), 
            ELSE(ratiomin))

    result = computeIntoFloats(op)
    # Convert back to ImagePlus
    img_white_corr = IL.wrap(result, 
                             "white corrected from dark (z: %i)" % zofwhite)
                             
    return img_white_corr


def process_tiles(implus_tiles_stack, channel, z, implus_white_corr, implus_dark_stack, metadata):
    """
    Compute:
    Im = (implus_tiles_stack - implus_dark_stack) / implus_white_corr

    for the given channel and z.

    Parameters:
    -----------
    - implus_tiles_stack: ImagePlus oject of imagej,
        The hyperstack of the tiles acquired with micro-manager v1.x

    - channel: int,
        The channel on which you want to do the computation

    - z: int,
        The z on which you want to do the computation

    - implus_white_corr: ImagePlus object of imagej,
        The image of the corrected white

    - implus_dark_stack: ImagePlus object of imagej,
        The stack containing the dark images for each channels

    - metadata: dict,
        The metadata of MM 1.4 or MM2

    return
    ------

    img_tiles_corr: ImagePlus Object,
        The stack of the corrected tiles
    """
    
    IJ.log("Process tiles (IMG-DARK)/CORRECTED_WHITE")
    # Extract the dark for the color position of the dataset:
    # TODO: Fix selecting channel when dark and white does not
    # have the same amount of filter
    dark = convertService.convert(implus_dark_stack, Dataset)
    if dark.numDimensions() > 2:
        dark = Views.hyperSlice(dark, 2, channel-1)
    else:
        # Need to apply crop on dark
        roi = getRoi(metadata)
        if roi is not None and len(roi) == 4 and implus_dark_stack.width > roi[2] and implus_dark_stack.height > roi[3]:
            xmin, ymin, xmax, ymax = roi
            xmax = xmin + xmax 
            ymax = ymin + ymax 
        else:
            xmin = ymin = 0
            xmax = implus_dark_stack.width
            ymax = implus_dark_stack.height
            
        
        dark = ops.transform().crop(dark, 
                              Intervals.createMinMax(xmin, ymin,
                                                           xmax-1, ymax-1))
                                                           
        IJ.log('dark is not an hyperslice, only one dark, keep this one')

    
    # Prepare the white
    white = convertService.convert(implus_white_corr, Dataset)

    # Extract the zposition and channel of the data-to-procced
    data = convertService.convert(implus_tiles_stack, Dataset)
    
    # Is there a z axis
    zpos = data.dimensionIndex(Axes.Z)
    chanpos = data.dimensionIndex(Axes.CHANNEL)
    xLen = data.dimension(data.dimensionIndex(Axes.X))
    yLen = data.dimension(data.dimensionIndex(Axes.Y))

    # Get the number of tiles (located in TIME axes)
    Ntiles = data.dimension(data.dimensionIndex(Axes.TIME))

    inter = None
    # Data are x,y,channel,z,t
    if data.numDimensions() == 5:
        inter = Intervals.createMinMax(0, 0, channel-1, z-1, 0,
                                       xLen-1, yLen-1, channel-1, z-1, Ntiles-1)
    # Data are x,y,channel,t
    if data.numDimensions() == 4:
        inter = Intervals.createMinMax(0, 0, channel-1, 0,
                                       xLen-1, yLen-1, channel-1, Ntiles-1)

    if inter is not None:
        data = ops.transform().crop(data, inter)


    # Operation
    op = [None] * Ntiles
    cpt = 0

    for tile in xrange(Ntiles):
        IJ.showProgress(tile, Ntiles)
        IJ.log('Compute %i / %i' % (tile, Ntiles))
        data_views = Views.hyperSlice(data, 2, tile)
        opsus = sub(data_views, dark)
        op[cpt] = IF(GT(opsus, 0.0), THEN(div(opsus, white)), ELSE(0.0))
        cpt += 1

    result = datasetService.create(Views.stack([o.view(FloatType()) for o in op]))
    result = ops.convert().uint16(result)

    # Create the ImagePlus from the result converted back to uint16
    name = metadata['Summary']['ChNames'][channel-1]
    imp_title = "Processed Tiles (channel: %i-%s, z: %i)" % (channel, name, z)
    img_tiles_corr = IL.wrap(result, imp_title)
    # Convert axis to Time and se the resolution
    img_tiles_corr.setDimensions(1, 1, Ntiles)
    img_tiles_corr.copyScale(implus_tiles_stack)
	
    # IJ.run(img_tiles_corr, "Properties...", "channels=1 slices=1 frames=%i" % Ntiles);
    return img_tiles_corr


def run_processing(channel, zimg, zwhite, run_basic=True, optimizeXY=True, savepath='./'):
    white_corr = process_white(white, dw, zwhite, roi)
    white_corr.show()

    proctiles = process_tiles(hstack, channel, zimg, white_corr, darks, metadata)
    proctiles.show()

    IJ.log("Save Tiles")
    # Save the image to the telemosMosaic forlder
    # Create the outputdirectory if it Not exist
    if not os.path.exists(savepath):
       os.makedirs(savepath)

    chname = metadata['Summary']['ChNames'][channel-1]
    # A folder for the processed tiles
    procfolder = savepath+'/DW_processed_tiles_%s_z%i_whitez%i' % (chname, zimg, zwhite)
    if not os.path.exists(procfolder):
       os.makedirs(procfolder)

    # Save images
    IJ.selectWindow(proctiles.getTitle())
    # New IJ version remove /tile_0001.tif after procfolder
    IJ.run("Image Sequence... ", "format=TIFF name=tile_ start=1 save=["+procfolder+"]")

    # Save the tile position file for the stitching
    with open(procfolder+'/TileConfiguration.txt', 'w') as f:
       f.write(tilesposfile)

    if run_basic:
        IJ.log("Run BASIC plugin")
        bstack = virtual_stack_to_stack(proctiles)
        bstack.show()
        IJ.run("BaSiC ", "processing_stack=["+bstack.getTitle()+"] flat-field=None dark-field=None shading_estimation=[Estimate shading profiles] shading_model=[Estimate flat-field only (ignore dark-field)] setting_regularisationparametes=Automatic temporal_drift=Ignore correction_options=[Compute shading and correct images] lambda_flat=0.50 lambda_dark=0.50");

        # Save image of basic
        basicfolder = savepath+'/Basic_DW_processed_tiles_%s_z%i_whitez%i' % (chname, zimg, zwhite)
        if not os.path.exists(basicfolder):
                os.makedirs(basicfolder)

        IJ.selectWindow('Corrected:'+bstack.getTitle())
        # Old way /tile_0001.tif
        IJ.run("Image Sequence... ", "format=TIFF name=tile_ start=1 save=["+basicfolder+"]")

        # Save TilePosition also
        with open(basicfolder+'/TileConfiguration.txt', 'w') as f:
           f.write(tilesposfile)


    IJ.log("Run stitching of tiles")
    # Run the stitching
    # Autre option si on a pas mal de ram sur l'ordinateur 'Save computation time (but use more RAM)'
    # Juste a enlever le compute overlap to use the raw file positions
    if optimizeXY:
       xyopt = 'compute_overlap'
    else:
       xyopt = ''

    IJ.run("Grid/Collection stitching", "type=[Positions from file] order=[Defined by TileConfiguration] directory=["+procfolder+"] layout_file=TileConfiguration.txt fusion_method=[Linear Blending] regression_threshold=0.30 max/avg_displacement_threshold=2.50 absolute_displacement_threshold=3.50 "+xyopt+" invert_x subpixel_accuracy computation_parameters=[Save memory (but be slower)] image_output=[Fuse and display]")
    IJ.run("Set... ", "zoom=33 x=1216 y=1212")
    #imt = WindowManager.getCurrentImage()
    #imt.setTitle("BW-processed-Fused.tif")
    IJ.saveAs("Tiff", procfolder+"/DW-processed-Fused-%s.tif" % chname)

    if run_basic:
       IJ.log("Run stitching of BASIC tiles")
       IJ.run("Grid/Collection stitching", "type=[Positions from file] order=[Defined by TileConfiguration] directory=["+basicfolder+"] layout_file=TileConfiguration.txt fusion_method=[Linear Blending] regression_threshold=0.30 max/avg_displacement_threshold=2.50 absolute_displacement_threshold=3.50 "+xyopt+" invert_x subpixel_accuracy computation_parameters=[Save memory (but be slower)] image_output=[Fuse and display]")
       IJ.run("Set... ", "zoom=33 x=1216 y=1212")
       IJ.saveAs("Tiff", basicfolder+"/Basic-corrected-DW-processed-Fused-%s.tif" % chname)

    # Some cleanups
    if run_basic:
       IJ.selectWindow('Corrected:'+bstack.getTitle())
       imt = WindowManager.getCurrentImage()
       bstack.close()
       imt.close()

    proctiles.close()

def run_gui(default_values=None):

    channels = metadata['Summary']['ChNames']
    dataZmax = metadata['Summary']['Slices']
    whiteZmax = white.getNSlices()

    if default_values is None:
       default_values = {}
       default_values['optimXY'] = False
       default_values['Basic'] = True
       default_values['dir'] = rootdir+'/IJ_roi%s_telemosMosaic' % (selectedroi)
       default_values['zimg'] = 0
       default_values['zwhite'] = whiteZmax/2
       default_values['channel'] = channels[0]

    gui = GenericDialogPlus("Mosaic Telemos")
    gui.addChoice("Select a Filter to Create the mozaic", channels, default_values['channel']) # Choice1 is default here
    gui.addSlider("Select z-plane in DATA images", 1, dataZmax, default_values['zimg'], 1)
    gui.addSlider("Select z-plane in WHITE images", 1, whiteZmax, default_values['zwhite'], 1)
    gui.addDirectoryField("Dossier de sauvegarde", default_values['dir'])
    gui.addCheckbox("Optimize tile positions (untick to use only XY from stage)", default_values['optimXY'])
    gui.addCheckbox("Use BASIC plugin to correct image stack after dark-white correction", default_values['Basic'])
    gui.setOKLabel("Build Mosaic")
    gui.setModal(False) # Allow live interaction (non-blocking)
    # gui.addDialogListener(MyListener())
    # gui.addWindowListener(MyWListener())
    gui.showDialog()

    szimg = gui.getSliders()[0]
    szwhite = gui.getSliders()[1]
    chbox = gui.getChoices()[0]
    foldert = gui.getStringFields()[0]
    XY = gui.getCheckboxes()[0]
    basic = gui.getCheckboxes()[1]

    while ((not gui.wasCanceled()) and not (gui.wasOKed())):

       # Update index if they have been changed from the stack
       hstack.setC(chbox.getSelectedIndex()+1)
       hstack.setZ(int(szimg.getValue()))
       white.setZ(int(szwhite.getValue()))

       # chbox.setSelectedIndex(hstack.getChannel())
       szwhite.setValue(white.getZ())

       Thread.sleep(500)

    if gui.wasOKed():
       channel = int(chbox.getSelectedIndex()+1)
       zimg = int(szimg.getValue())
       zwhite = int(szwhite.getValue())
       outdir = foldert.getText()

       use_basic = basic.getState()
       optimizeXY = XY.getState()

       # Backup the used values to relaunch the gui with the same options
       default_values = {}
       default_values['optimXY'] = optimizeXY
       default_values['Basic'] = use_basic
       default_values['dir'] = outdir
       default_values['zimg'] = zimg
       default_values['zwhite'] = zwhite
       default_values['channel'] = channels[channel-1]

       run_processing(channel, zimg, zwhite, run_basic=use_basic, optimizeXY=optimizeXY, savepath=outdir)

       run_gui(default_values)

    else:
       hstack.close()
       darks.close()
       dw.close()
       white.close()


def getMMtiles(metadata, selected_roi, tilesondisk):
    """
    Extract tiles position from micromanager metadata

    tilesondisk, list of tiles name found on disk
    """
    
    mmversion = getMMversion(metadata)

    mmsum = metadata['Summary']
    xt = None
    yt = None
    
    if mmversion == 1:
        # Positions
        xt = [None] * len(mmsum['InitialPositionList'])
        yt = [None] * len(mmsum['InitialPositionList'])
        test_nameSE = 'roi'+selected_roi
        test_nameGC = '%s-Pos_' % selected_roi

        uniquecols = set(a['GridColumnIndex'] for a in mmsum['InitialPositionList'])
        uniquerows = set(a['GridRowIndex'] for a in mmsum['InitialPositionList'])
        Nrow = max(uniquerows) + 1
        Ncol = max(uniquecols) + 1

        for i, pl in enumerate(mmsum['InitialPositionList']):
            if pl['Label'] in tilesondisk:
                # print(pl['Label'])
                if test_nameSE in pl['Label']:
                    ind = i

                if test_nameGC in pl['Label']:
                    # Compute the globel indice ind = col*(Nrow)+row
                    # as the order is not the same as the one loaded by bio-format reader
                    c = pl['GridColumnIndex']
                    r = pl['GridRowIndex']
                    ind = c * Nrow + r
                    # print(ind, pl['Label'])

                xt[ind] = pl['DeviceCoordinatesUm']['XYStage'][0]
                yt[ind] = pl['DeviceCoordinatesUm']['XYStage'][1]

            else:
                IJ.log("[WARNING]: File %s listed on metadata does not exist on disk" % pl['Label'])

    else:
        # for MM2
        xt = [None] * len(mmsum['StagePositions'])
        yt = [None] * len(mmsum['StagePositions'])
        test_nameSE = 'roi'+selected_roi
        test_nameGC = '%s-Pos_' % selected_roi

        uniquecols = set(a['GridCol'] for a in mmsum['StagePositions'])
        uniquerows = set(a['GridRow'] for a in mmsum['StagePositions'])
        Nrow = max(uniquerows) + 1
        Ncol = max(uniquecols) + 1

        for i, pl in enumerate(mmsum['StagePositions']):
            if pl['Label'] in tilesondisk:
                # print(pl['Label'])
                if test_nameSE in pl['Label']:
                    ind = i

                if test_nameGC in pl['Label']:
                    # Compute the globel indice ind = col*(Nrow)+row
                    # as the order is not the same as the one loaded by bio-format reader
                    c = pl['GridCol']
                    r = pl['GridRow']
                    ind = c * Nrow + r
                    # print(ind, pl['Label'])

                xt[ind] = pl['DevicePositions'][0]['Position_um'][0]
                yt[ind] = pl['DevicePositions'][0]['Position_um'][1]

            else:
                IJ.log("[WARNING]: File %s listed on metadata does not exist on disk" % pl['Label'])

    return xt, yt
    
def extract_tile_positions(metadata, selected_roi, tilesondisk, tilename='tile_'):
    """
    Extraction des positions des tuiles depuis les métadonnées
    pour les rendres compatible avec le plugin de stitching.
    """
    
    IJ.log("Extract tiles coordinates from metadata")
    # Extract pixel size
    pix2um = getScale(metadata)
    # The roi width
    roiwidth, _ = getWidthHeight(metadata)

    # Is the microscope use Andor camera ?
    i = getFirstMetadata(metadata)
    if metadata[metadata.keys()[i]]['Camera'] == 'Andor':
        ACAMAS = True
    else:
        ACAMAS = False
        
    xt, yt = getMMtiles(metadata, selected_roi, tilesondisk)
    
    if ACAMAS:
        xtmp = yt
        ytmp = xt
        xt = xtmp
        yt = ytmp
        #xt = [-x for x in xtmp]
        #yt = [-y for y in ytmp]        
        
    # Remove None values in xt and yt (when the tile is not on disk)
    igood = [i for i,v in enumerate(xt) if v is not None]
    xt = [xt[i] for i in igood]
    yt = [yt[i] for i in igood]

    if ACAMAS:
        xt = [-x for x in xt]
        
    xt = map(lambda x: x/pix2um, xt)
    yt = map(lambda x: x/pix2um, yt)

    xt = map(lambda x: x-int(roiwidth/2), xt)
    yt = map(lambda x: x-int(roiwidth/2), yt)

    xt = [x-xt[0] for x in xt]
    yt = [y-yt[0] for y in yt]

        
    outstr = []
    for i in range(len(yt)):
        outstr += ['%s%04i.tif; ; (%0.2f, %0.2f)'%(tilename, i+1, yt[i], -xt[i])]

    # Build the file
    out = '# Define the number of dimensions we are working on\n'
    out += 'dim = 2\n\n'
    out += '# Define the image coordinates (in pixels)\n'
    out += '\n'.join(outstr)

    return out


if __name__ in ['__builtin__','__main__']:
    # Get path from gui
    fdata = fdata.absolutePath
    fdark = fdark.absolutePath
    fdarkwhite = fdarkwhite.absolutePath
    fwhite = fwhite.absolutePath

    # Start Log
    IJ.log("------- Start MOSAIC TELEMOS -------")
    IJ.log("Image stack: %s" % fdata)
    IJ.log("Dark stack: %s" % fdark)
    IJ.log("Dark of white stack: %s" % fdarkwhite)
    IJ.log("White stack: %s" % fwhite)
    
    # LOAD DATA
    hstack, metadata, rootdir, selectedroi, file_names = load_image_tiles_stack(fdata)
    hstack.show()

	# GET the version of MM
    mmversion = getMMversion(metadata)

    # LOAD ROI info
    roi = getRoi(metadata, mmversion)
    IJ.log('ROI: %s' % str(roi))
    
    # Make a list of tiles names
    tilesondisk = set(os.path.dirname(t) for t in file_names)
    tilesposfile = extract_tile_positions(metadata, selectedroi, tilesondisk)

    darks = load_images(fdark, roi=roi)
    darks.setTitle("dark")
    darks.show()

    dw = load_images(fdarkwhite, roi=roi)
    dw.setTitle('dark-white')
    dw.show()
    IJ.run("Set... ", "zoom=50 x=512 y=512");

    white = load_images(fwhite, roi=roi, virtual=True)
    white.setTitle('white')
    white.show()
    IJ.run("Set... ", "zoom=50 x=512 y=512");

    run_gui()
