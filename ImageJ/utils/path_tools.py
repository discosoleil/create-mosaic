# -*- coding: utf-8 -*-

"""
This file is part of create-mosaic toolbox.

It's a collection of python (compatible with jython) function to deal with paths
and paths with templates.

@Author: H. chauvet (for DISCO beamline @ Synchrotron-Soleil)
"""

import glob
import re
import os
from string import Formatter


def to_regex_template(python_template):
    """
    remove special formating from python to make them compatible with regex
    searching. For example {toto:003d} -> {toto}
    """
    parsed_tmpl = Formatter().parse(python_template)

    regex_tmpl = ''
    for k in parsed_tmpl:
        if k[0] is not None:
            regex_tmpl += k[0]

        if k[1] is not None:
            regex_tmpl += '{%s}' % k[1]

    return regex_tmpl


def get_template_parser(python_template):
    """
    Function tha return converter for python template pattern type (like 003d,
    0.2f, %) to the correct python type (int, float, str)
    """
    parsed_tmpl = Formatter().parse(python_template)

    parser = {}
    for k in parsed_tmpl:
        if k[1] is not None:
            if k[2] == '':
                if k[1] not in parser:
                    parser[k[1]] = str
            else:
                if k[2].lower()[-1] in ['d']:
                    if k[1] not in parser or parser[k[1]] is str:
                        parser[k[1]] = int
                elif k[2].lower()[-1] in ['f', 'e', 'g', 'n', '%']:
                    if k[1] not in parser or parser[k[1]] is str:
                        parser[k[1]] = float
                else:
                    parser[k[1]] = str

    return parser


def find_groups_values(path_template, order=True, unique=True):
    """
    find all possible values for each groups of the pattern. The pattern use
    the default format syntax of python. You could use formater as {z:003d}
    for instance.

    Example:

    path_template = './Zdir_{z}/myfile_{z:003d}-{x}-{y}.tif'

    # the function will loop over all files starting with myfile and
    # return possibles values for each groups

    gv = find_groups_values(path_template)

    # gv['z'] -> [0, 1, 2]
    # gv['x'] -> ['toto', 'tata']
    # gv['y'] -> ['100']
    """

    # Find how many group and their names are in the python strings pattern
    group_names = [i[1] for i in Formatter().parse(path_template) if i[1] is not None]

    # Init output dict
    groups_values = {k: [] for k in group_names}

    re_tmpl = to_regex_template(path_template)

    # Find all files
    allfiles = glob.glob(re_tmpl.format(**{k: '*' for k in group_names}))

    # Create the regexp pattern
    re_pattern = r'^'+re_tmpl.format(**{k: '(.*)' for k in group_names})

    # Find group parser
    groups_parser = get_template_parser(path_template)

    # Search groups values using regexp
    # Fix windows path in doubling \ in the regex pattern
    if os.sep == '\\':
    	re_pattern = re_pattern.replace(os.sep, os.sep+os.sep)
        
    r = re.compile(re_pattern)
    for m in [r.match(f) for f in allfiles]:
        groups = m.groups()
        for i, v in enumerate(groups):
            if v not in groups_values[group_names[i]]:
                groups_values[group_names[i]] += [groups_parser[group_names[i]](v)]

    if unique:
        for k in groups_values.keys():
            groups_values[k] = set(groups_values[k])

    if order:
        for k in groups_values.keys():
            groups_values[k] = sorted(groups_values[k])

    return groups_values
