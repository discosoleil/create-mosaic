#@ File (label="Root directory of image", style="directory") rootdir
#@ String (label="file pattern", description="Give the file pattern mydata_{z:d}_{x:003d}_{filter}") filepattern
#@ ConvertService convertService

"""
A way to load any series of .tif in several folders as a virtualstack.
"""

import os
from ij import ImagePlus, VirtualStack, IJ  
from net.imglib2.img.display.imagej import ImageJFunctions as IL
import sys

if __name__ == '__main__':
    # Add basedir to path
    basedir = os.path.dirname(__file__)
    sys.path.append(basedir)
    # Load the image sequence loader
    from utils.ImagesLoaders import load_image_sequence

    #filepath = '/home/hugo/Documents/Boulo/ligne_DISCO/20200160/Pn_long_100x_1530tiles_1/'
    filepath = rootdir.absolutePath
    #path_pattern = 'roi6_tile{tile:d}/img_000000000_{filter}_{z:003d}.tif'
    imp, fn = load_image_sequence(filepath, filepattern, return_filenames=True)

    imp.show()
