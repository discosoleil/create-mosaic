#@ OpService ops
#@ Dataset data

# to run this tutorial run 'file->Open Samples->Confocal Series' and make sure that
# confocal-series.tif is the active image


from net.imglib2.util import Intervals
from net.imagej.axis import Axes
from net.imglib2.img.display.imagej import ImageJFunctions as IL
from net.imglib2.algorithm.math.ImgMath import computeIntoFloat, sub, div, GT, LT, IF, THEN, ELSE, minimum
from net.imglib2.view import Views 
from net.imglib2.converter import Converters
from net.imglib2.img.array import ArrayImgs  
from net.imglib2.view import Views 

from ij import IJ, ImagePlus

# first take a look at the size and type of each dimension
for d in range(data.numDimensions()):
	print "axis d: type: "+str(data.axis(d).type())+" length: "+str(data.dimension(d))

img=data.getImgPlus()
print(img.numDimensions())
print(img.dimensionIndex(Axes.CHANNEL), img.dimensionIndex(Axes.Z), img.dimensionIndex(Axes.TIME))

xLen = data.dimension(data.dimensionIndex(Axes.X))
yLen = data.dimension(data.dimensionIndex(Axes.Y))
zLen = data.dimension(data.dimensionIndex(Axes.Z))
tLen = data.dimension(data.dimensionIndex(Axes.TIME))
cLen = data.dimension(data.dimensionIndex(Axes.CHANNEL))

# crop a channel
c0=ops.transform().crop(img, Intervals.createMinMax(0, 0, 3, 5, 0, xLen-1, yLen-1, 3, 5, tLen-1))
converted = ops.convert().float32(c0)
print(converted.numDimensions())

minvalue = 6  
op1 = sub(converted, 750)
op = IF(LT(op1, 0), THEN(minvalue), ELSE(op1))

res = computeIntoFloat(op)
res = ops.convert().uint16(res)

#print(ops.op("stat.min",res))
#IL.show(converted)
aaa=IL.wrap(res, "ImgMath view")
aaa.show() 
print(type(aaa))
one = Views.interval(res, Intervals.createMinMax(0, 0, 3, xLen-1, yLen-1, 3))

resa = float(ops.run("stats.max", one).toString())
print(resa)
IL.show(one)